﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPay_Model;

namespace InstantPayServiceLib
{
    public static class InstantPay_PayoutService
    {
        private static string TokenID = "ffecc13de31e6be9fe916654e0bcf61f";//"  3470f9330a3cc01d71f31a7050ad1834"
        private static string PostUrl { get; set; }
        // private static int OutletId = 1;

        #region [DMT-DIRECT]
        public static DataTable GetDMTDirectRemitter(string mobile, string agentid, string remitterid = null)
        {
            return InstantPay_PayoutDatabase.GetDMTDirectRemitter(mobile, agentid, remitterid);
        }

        public static bool InsertDMTDirectRemitter(string mobile, string fistname, string lastname, string pincode, string address, string agentid, string dynremitterid, ref string otp)
        {
            return InstantPay_PayoutDatabase.InsertDMTDirectRemitter(mobile, fistname, lastname, pincode, address, agentid, dynremitterid, ref otp);
        }

        public static bool ResendDMTDOtp(string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.ResendDMTDOtp(agentid, mobile, remitterid);
        }

        public static bool VerifyingDMTDRemitter(string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.VerifyingDMTDRemitter(agentid, mobile, remitterid);
        }
        #endregion

        private static string GetInitiatePayoutPostUrl()
        {
            return "https://www.instantpay.in/ws/payouts/";
        }

        public static bool CheckPayoutKycComplete(string agentid, string mobile, string remitterid, ref string regId, ref bool showkyc, ref string kycStatus)
        {
            return InstantPay_PayoutDatabase.CheckPayoutKycComplete(agentid, mobile, remitterid, ref regId, ref showkyc, ref kycStatus);
        }

        public static bool GenrateOTPForTransfer(string regId, string agentid, string mobile, string remitterid, ref string otp)
        {
            return InstantPay_PayoutDatabase.GenrateOTPForTransfer(regId, agentid, mobile, remitterid, ref otp);
        }
        public static bool RemoveOTPForTransfer(string regId, string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.RemoveOTPForTransfer(regId, agentid, mobile, remitterid);
        }

        public static bool GenrateOTPForBenDelete(string agentid, string mobile, string remitterid, string benid, ref string otp)
        {
            return InstantPay_PayoutDatabase.GenrateOTPForBenDelete(agentid, mobile, remitterid, benid, ref otp);
        }

        public static DataTable GetPayoutRemitterDetails(string regId, string agentid, string mobile, string remitterid, ref DataTable BenDetail, string benid = null)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterDetails(regId, agentid, mobile, remitterid, ref BenDetail, benid);
        }

        public static DataTable GetPayoutRemitterBenDetails(string regId, string agentid, string mobile, string remitterid, string benid = null)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterBenDetails(regId, agentid, mobile, remitterid, benid);
        }

        public static bool UpdateRemitterKYCDetails(string regId, string agentId, string mobile, string remitterId, string fn, string ln, string add, string gen, string dob, string proof, string pno, string frontImg, string backImg)
        {
            return InstantPay_PayoutDatabase.UpdateRemitterKYCDetails(regId, agentId, mobile, remitterId, fn, ln, add, gen, dob, proof, pno, frontImg, backImg);
        }

        public static bool PayoutUpdateLocalAddress(string regid, string remitterId, string agentId, string mobile, string address)
        {
            return InstantPay_PayoutDatabase.PayoutUpdateLocalAddress(regid, remitterId, agentId, mobile, address);
        }

        public static bool PayoutBeneficiaryRegistration(string regid, string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            return InstantPay_PayoutDatabase.PayoutBeneficiaryRegistration(regid, remitterid, name, mobile, account, bankname, ifsc, agentId);
        }

        public static string PaayoutDirectMoneyTransfer(string RegId, string agentId, string mobile, string remitterId, string benid, string transtype, string clientRefId, string amount, string ipadd, string remtRemark, string alert_email, string alert_mobile)
        {
            string result = string.Empty;

            try
            {
                //DataTable BenDetail = InstantPay_PayoutDatabase.GetPayoutRemitterBenDetails(RegId, agentId, mobile, remitterId, benid);
                DataTable BenDetail = LedgerService.GetRemBenDetailByAgencyId(agentId, mobile, remitterId, benid);

                if (BenDetail != null && BenDetail.Rows.Count > 0)
                {
                    string accountno = BenDetail.Rows[0]["AccountNo"].ToString().Trim();
                    string ifsccode = BenDetail.Rows[0]["IFSCCode"].ToString().Trim();
                    string bankname = BenDetail.Rows[0]["BankName"].ToString().Trim();
                    string bene_name = BenDetail.Rows[0]["BenName"].ToString().Replace("S/O", "").Replace("s/o", "").Trim();

                    string latitude = "28.690430";
                    string longitude = "77.101662";
                    string ipaddress = ipadd;

                    string remarks = "FundTransfer";

                    string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\","
                        + "\"bene_name\":\"" + bene_name + "\",\"credit_amount\":\"" + amount + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alert_mobile + "\",\"alert_email\":\"" + alert_email + "\","
                        + "\"remarks\":\"" + remarks + "\"}}";

                    PostUrl = GetInitiatePayoutPostUrl() + "direct";

                    InitiatePayout payout = new InitiatePayout();
                    payout.AgentId = agentId;
                    payout.RemitterMobile = mobile;
                    payout.sp_key = transtype;
                    payout.bene_name = bene_name;
                    payout.credit_amount = amount;
                    payout.latitude = latitude;
                    payout.longitude = longitude;
                    payout.endpoint_ip = ipaddress;
                    payout.alert_mobile = alert_mobile;
                    payout.alert_email = alert_email;
                    payout.otp_auth = "";
                    payout.otp = "";
                    payout.remarks = remarks;
                    payout.TrackId = clientRefId;
                    payout.RemitterId = remitterId;
                    payout.bank_name = bankname;
                    payout.PayType = "payout_direct";
                    payout.RemtRemark = remtRemark;
                    payout.LedgerAmount = amount;
                    payout.BenificieryId = benid;

                    int payoutId = InsertInitiatePayout(payout);

                    result = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId);
                    //string response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";

                    if (!string.IsNullOrEmpty(result))
                    {
                        dynamic dyResult = JObject.Parse(result);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic dypayout = dyData.payout;

                            InitiatePayout uppayout = new InitiatePayout();

                            uppayout.InitiatePayoutId = payoutId;
                            uppayout.external_ref = dyData.external_ref;
                            uppayout.ipay_id = dyData.ipay_id;
                            uppayout.transfer_value = dyData.transfer_value;
                            uppayout.type_pricing = dyData.type_pricing;
                            uppayout.commercial_value = dyData.commercial_value;
                            uppayout.value_tds = dyData.value_tds;
                            uppayout.ccf = dyData.ccf;
                            uppayout.vendor_ccf = dyData.vendor_ccf;
                            uppayout.charged_amt = dyData.charged_amt;
                            uppayout.payout_credit_refid = dypayout.credit_refid;
                            uppayout.payout_account = dypayout.account;
                            uppayout.payout_ifsc = dypayout.ifsc;
                            uppayout.payout_name = dypayout.name;
                            uppayout.timestamp = dyResult.timestamp;
                            uppayout.ipay_uuid = dyResult.ipay_uuid;
                            uppayout.orderid = dyResult.orderid;
                            uppayout.environment = dyResult.environment;
                            uppayout.Status = statusMessage;

                            if (InstantPay_DataBase.UpdateInitiatePayout(uppayout))
                            {
                                return result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static int InsertInitiatePayout(InitiatePayout payout)
        {
            try
            {
                return InstantPay_DataBase.InsertInitiatePayout(payout);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string trackid)
        {
            string ReturnValue = string.Empty;
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, trackid);

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                InstantPay_DataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, trackid);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, trackid);
            return ReturnValue;
        }

        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            return InstantPay_PayoutDatabase.GetFilterTransactionHistory(remitterId, fromDate, toDate, trackId, filstatus);
        }

        public static bool DeleteBeneficiaryDetailById(string benid)
        {
            try
            {
                return InstantPay_PayoutDatabase.DeleteBeneficiaryDetailById(benid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetCommonFundTransHistory(string trackid, string agentId, ref DataTable RemitterDel, ref DataTable BenDetail)
        {
            return InstantPay_PayoutDatabase.GetCommonFundTransHistory(trackid, agentId, ref RemitterDel, ref BenDetail);
        }

        #region [Ledger Section Credit and Debit]
        public static List<string> PayoutLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string user_Id, string mobile, string remitterId, string benid)
        {
            return LedgerService.PayoutLedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, user_Id, mobile, remitterId, benid);
        }
        #endregion

        //=====================================================================

        public static DataTable GetPayoutRemitterWithBeniCheck(string transId, string trackId, string regId, string agentid, string mobile, string remitterid, ref DataTable dtTrans)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterWithBeniCheck(transId, trackId, regId, agentid, mobile, remitterid, ref dtTrans);
        }
    }
}
