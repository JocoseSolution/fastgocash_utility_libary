﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public static class LedgerDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(InstantPayConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static List<string> DMTLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId, string benid)
        {
            List<string> HS = new List<string>();

            try
            {
                string commisionType = string.Empty;
                string charges = string.Empty;
                double applyCharge = 0;
                string agencyCreditLimit = string.Empty;
                Amount = Math.Abs(Amount);

                //DataTable dtAgency = InstantPay_DataBase.GetAgencyDetailById(AgentId);
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetDMTRemBenDetailByAgencyId(AgentId, mobile, remitterId, benid);
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();
                    string groupType = dtAgencyRemtBen.Rows[0]["Agent_Type"].ToString();

                    DataTable dtMarkup = InstantPay_DataBase.GetDMTCommision(Amount, groupType);
                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        commisionType = dtMarkup.Rows[0]["Charges_Type"].ToString();
                        charges = dtMarkup.Rows[0]["Charges"].ToString();

                        //double reminder = Math.Ceiling(Amount / Convert.ToDouble(commamount));

                        if (commisionType.ToLower() == "fixed")
                        {
                            applyCharge = Convert.ToDouble(charges);
                        }
                        else
                        {
                            applyCharge = (Amount * Convert.ToDouble(charges)) / 100;
                        }
                    }

                    Amount = Amount + applyCharge;
                    if (Uploadtype.Trim().ToLower() == "dr")
                    {
                        Debit = Debit + applyCharge;

                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(Debit))
                        {
                            sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                            sqlCommand.CommandType = CommandType.StoredProcedure;

                            sqlCommand.Parameters.AddWithValue("@Amount", Amount);
                            sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                            sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                            sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                            sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                            sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                            sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                            sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                            sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                            sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                            sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                            sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                            sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                            sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                            sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                            sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                            sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                            sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                            sqlCommand.Parameters.AddWithValue("@ID", 1);
                            sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                            sqlCommand.Parameters.AddWithValue("@Type", "Con");
                            sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                            sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                            sqlCommand.Parameters.AddWithValue("@BankName", "");
                            sqlCommand.Parameters.AddWithValue("@BankCode", "");
                            sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                            sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                            sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                            sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                            //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                            sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                            MyAmdOpenConnection();
                            int isSuccess = sqlCommand.ExecuteNonQuery();
                            double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                            string result = sqlCommand.Parameters["@result"].Value.ToString();
                            MyAmdCloseConnection();

                            HS.Add(result);
                            HS.Add(Aval_Balance.ToString());
                        }
                    }
                    else
                    {
                        Credit = Credit + applyCharge;

                        sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.Parameters.AddWithValue("@Amount", Amount);
                        sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                        sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                        sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                        sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                        sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                        sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                        sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                        sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                        sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                        sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                        sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                        sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                        sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                        sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                        sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                        sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                        sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                        sqlCommand.Parameters.AddWithValue("@ID", 1);
                        sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                        sqlCommand.Parameters.AddWithValue("@Type", "Con");
                        sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                        sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                        sqlCommand.Parameters.AddWithValue("@BankName", "");
                        sqlCommand.Parameters.AddWithValue("@BankCode", "");
                        sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                        sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                        sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                        sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                        //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                        sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                        MyAmdOpenConnection();
                        int isSuccess = sqlCommand.ExecuteNonQuery();
                        double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                        string result = sqlCommand.Parameters["@result"].Value.ToString();
                        MyAmdCloseConnection();

                        HS.Add(result);
                        HS.Add(Aval_Balance.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        public static List<string> PayoutLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string user_Id, string mobile, string remitterId, string benid)
        {
            List<string> HS = new List<string>();

            try
            {
                string commisionType = string.Empty;
                string commamount = string.Empty;
                string charges = string.Empty;
                double applyCharge = 0;
                string agencyCreditLimit = string.Empty;
                Amount = Math.Abs(Amount);

                //DataTable dtAgency = InstantPay_DataBase.GetAgencyDetailById(AgentId);
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetRemBenDetailByAgencyId(user_Id, mobile, remitterId, benid);
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();
                    string groupType = dtAgencyRemtBen.Rows[0]["Agent_Type"].ToString();

                    DataTable dtMarkup = InstantPay_PayoutDatabase.GetPayoutCommision(groupType);

                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        commisionType = dtMarkup.Rows[0]["Charges_Type"].ToString();
                        commamount = dtMarkup.Rows[0]["Amount"].ToString();
                        charges = dtMarkup.Rows[0]["Charges"].ToString();

                        double reminder = Math.Ceiling(Amount / Convert.ToDouble(commamount));

                        if (commisionType.ToLower() == "fixed")
                        {
                            if (reminder > 1)
                            {
                                applyCharge = reminder * Convert.ToDouble(charges);
                            }
                            else
                            {
                                applyCharge = Convert.ToDouble(charges);
                            }
                        }
                        else
                        {
                            if (reminder > 1)
                            {
                                applyCharge = ((Amount * Convert.ToDouble(charges)) / 100) * reminder;
                            }
                            else
                            {
                                applyCharge = (Amount * Convert.ToDouble(charges)) / 100;
                            }
                        }
                    }

                    Amount = Amount + applyCharge;

                    if (Uploadtype.Trim().ToLower() == "dr")
                    {
                        Debit = Debit + applyCharge;

                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(Debit))
                        {
                            sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                            sqlCommand.CommandType = CommandType.StoredProcedure;

                            sqlCommand.Parameters.AddWithValue("@Amount", Amount);
                            sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                            sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                            sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                            sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                            sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                            sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                            sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                            sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                            sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                            sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                            sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                            sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                            sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                            sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                            sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                            sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                            sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                            sqlCommand.Parameters.AddWithValue("@ID", 1);
                            sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                            sqlCommand.Parameters.AddWithValue("@Type", "Con");
                            sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                            sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                            sqlCommand.Parameters.AddWithValue("@BankName", "");
                            sqlCommand.Parameters.AddWithValue("@BankCode", "");
                            sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                            sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                            sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                            sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                            //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                            sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                            MyAmdOpenConnection();
                            int isSuccess = sqlCommand.ExecuteNonQuery();
                            double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                            string result = sqlCommand.Parameters["@result"].Value.ToString();
                            MyAmdCloseConnection();

                            HS.Add(result);
                            HS.Add(Aval_Balance.ToString());
                        }
                    }
                    else
                    {
                        Credit = Credit + applyCharge;

                        sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.Parameters.AddWithValue("@Amount", Amount);
                        sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                        sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                        sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                        sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                        sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                        sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                        sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                        sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                        sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                        sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                        sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                        sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                        sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                        sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                        sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                        sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                        sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                        sqlCommand.Parameters.AddWithValue("@ID", 1);
                        sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                        sqlCommand.Parameters.AddWithValue("@Type", "Con");
                        sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                        sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                        sqlCommand.Parameters.AddWithValue("@BankName", "");
                        sqlCommand.Parameters.AddWithValue("@BankCode", "");
                        sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                        sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                        sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                        sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                        //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                        sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                        MyAmdOpenConnection();
                        int isSuccess = sqlCommand.ExecuteNonQuery();
                        double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                        string result = sqlCommand.Parameters["@result"].Value.ToString();
                        MyAmdCloseConnection();

                        HS.Add(result);
                        HS.Add(Aval_Balance.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        public static List<string> LedgerDebitCreditForCheckBen_DetailOnline(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId)
        {
            List<string> HS = new List<string>();

            try
            {
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetRemBenDetailByAgencyId(AgentId, mobile, remitterId, "");
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Amount", Math.Abs(Amount));
                    sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                    sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                    sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                    sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                    sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                    sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                    sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                    sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                    sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                    sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                    sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                    sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                    sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                    sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                    sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                    sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                    sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                    sqlCommand.Parameters.AddWithValue("@ID", 1);
                    sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                    sqlCommand.Parameters.AddWithValue("@Type", "Con");
                    sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                    sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                    sqlCommand.Parameters.AddWithValue("@BankName", "");
                    sqlCommand.Parameters.AddWithValue("@BankCode", "");
                    sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                    sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                    sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                    sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                    //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                    sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                    string result = sqlCommand.Parameters["@result"].Value.ToString();
                    MyAmdCloseConnection();

                    HS.Add(result);
                    HS.Add(Aval_Balance.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        public static List<string> LedgerDMTDebitCreditForCheckBen_DetailOnline(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId)
        {
            List<string> HS = new List<string>();

            try
            {
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetDMTRemBenDetailByAgencyId(AgentId, mobile, remitterId, "");
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Amount", Math.Abs(Amount));
                    sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                    sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                    sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                    sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                    sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                    sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                    sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                    sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                    sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                    sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                    sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                    sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                    sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                    sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                    sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                    sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                    sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                    sqlCommand.Parameters.AddWithValue("@ID", 1);
                    sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                    sqlCommand.Parameters.AddWithValue("@Type", "Con");
                    sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                    sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                    sqlCommand.Parameters.AddWithValue("@BankName", "");
                    sqlCommand.Parameters.AddWithValue("@BankCode", "");
                    sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                    sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                    sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                    sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                    //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                    sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                    string result = sqlCommand.Parameters["@result"].Value.ToString();
                    MyAmdCloseConnection();

                    HS.Add(result);
                    HS.Add(Aval_Balance.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        public static List<string> LedgerDebitCreditUtility(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            List<string> HS = new List<string>();

            try
            {
                sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Amount", Math.Abs(Amount));
                sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
                sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
                sqlCommand.Parameters.AddWithValue("@PnrNo", "");
                sqlCommand.Parameters.AddWithValue("@TicketNo", "");
                sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
                sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
                sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
                sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
                sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                sqlCommand.Parameters.AddWithValue("@Debit", Debit);
                sqlCommand.Parameters.AddWithValue("@Credit", Credit);
                sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
                sqlCommand.Parameters.AddWithValue("@Remark", Remark);
                sqlCommand.Parameters.AddWithValue("@PaxId", 0);
                sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
                sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
                sqlCommand.Parameters.AddWithValue("@ID", 1);
                sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
                sqlCommand.Parameters.AddWithValue("@Type", "Con");
                sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
                sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
                sqlCommand.Parameters.AddWithValue("@BankName", "");
                sqlCommand.Parameters.AddWithValue("@BankCode", "");
                sqlCommand.Parameters.AddWithValue("@Narration", Narration);
                sqlCommand.Parameters.AddWithValue("@TransType", TransType);
                sqlCommand.Parameters.AddWithValue("@ModuleType", "");
                sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
                sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
                string result = sqlCommand.Parameters["@result"].Value.ToString();
                MyAmdCloseConnection();

                HS.Add(result);
                HS.Add(Aval_Balance.ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }
    }
}
