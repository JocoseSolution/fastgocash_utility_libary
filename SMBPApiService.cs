﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static InstantPayServiceLib.SMBPModel;

namespace InstantPayServiceLib
{
    public static class SMBPApiService
    {
        private static string ClientID = "1000000019"; //"100059620";
        private static string SecretKey = "GWND#!@43D2@";//"208052#2019";
        private static string TokenID = "GANDHI@1QWR5E"; //"D6DE4E33-9CBC-4F80-9280-F1E86671862F";
        private static string PostUrl = "https://mswift.quicksekure.com/api/BBPS/"; //"http://staging.quicksekure.com/api/BBPS/";

        //private static string ClientID = "100059620";
        //private static string SecretKey = "208052#2019";
        //private static string TokenID = "D6DE4E33-9CBC-4F80-9280-F1E86671862F";
        //private static string PostUrl = "http://staging.quicksekure.com/api/BBPS/";

        #region [Data Base Section]
        public static string GetMobileOprator(string serviceType, string selectname)
        {
            string result = string.Empty;

            DataTable dtOprator = SMBPDataBase.GetMobileOprator(serviceType);

            if (dtOprator != null && dtOprator.Rows.Count > 0)
            {
                List<string> iscontain = new List<string>();

                result = "<option value='0'>--Select " + selectname + "--</option>";
                for (int i = 0; i < dtOprator.Rows.Count; i++)
                {
                    if (!iscontain.Contains(dtOprator.Rows[i]["Operator"].ToString()))
                    {
                        result = result + "<option value='" + dtOprator.Rows[i]["SpKey"].ToString() +
                        "' data-isbillfetch='" + dtOprator.Rows[i]["IsBillFetch"].ToString() + "' data-billupdation='" + dtOprator.Rows[i]["BillUpdation"].ToString() + "' data-fetchid='" + dtOprator.Rows[i]["FetchId"].ToString() + "'>"
                        + dtOprator.Rows[i]["Operator"].ToString() + "</option>";

                        iscontain.Add(dtOprator.Rows[i]["Operator"].ToString());
                    }
                }
            }

            return result;
        }
        #endregion


        #region [API Section]
        public static string BBPSBillFetch(string agentId, string number, string spKey, string circleID, List<string> optional = null)
        {
            string result = string.Empty;

            try
            {
                string clientRefId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                string reqJson = "{\"ClientRefId\" :\"" + clientRefId + "\",\"Number\" :\"" + number + "\",\"SPKey\" :\"" + spKey + "\",\"TelecomCircleID\" :\"" + circleID + "\"";

                if (optional != null && optional.Count > 0)
                {
                    int counter = 1;
                    foreach (var opt in optional)
                    {
                        reqJson = reqJson + ",\"Optional" + counter + "\" :\"" + opt + "\"";

                        counter = counter + 1;
                    }
                }

                reqJson = reqJson + "}";

                result = Post("POST", (PostUrl + "BBPSBillFetch"), reqJson, "BBPSBillFetch", agentId, clientRefId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static string BBPSPayment(string agentId, string clientRefId, string number, string spKey, string circleID, string amount, List<string> optional = null, string customerName = null)
        {
            string result = string.Empty;

            try
            {
                string reqJson = "{\"ClientRefId\" :\"" + clientRefId + "\",\"Number\" :\"" + number + "\",\"SPKey\" :\"" + spKey + "\",\"TelecomCircleID\" :\"" + circleID + "\",\"Amount\" :\"" + amount + "\"";

                if (optional != null && optional.Count > 0)
                {
                    int counter = 1;
                    foreach (var opt in optional)
                    {
                        reqJson = reqJson + ",\"Optional" + counter + "\" :\"" + opt + "\"";

                        counter = counter + 1;
                    }
                }

                reqJson = reqJson + "}";

                int lastId = SMBPDataBase.InsertBBPSPayment(agentId, clientRefId, number, spKey, circleID, amount, optional, customerName);

                result = Post("POST", (PostUrl + "BBPSPayment"), reqJson, "BBPSPayment", agentId, clientRefId);

                if (!string.IsNullOrEmpty(result))
                {
                    dynamic rech = JObject.Parse(result);

                    string responseCode = rech.ResponseCode;

                    //bool refund = false; string refundid = "";
                    //if (responseCode != "000" && responseCode != "999")
                    //{
                    //    refundid = "RUF" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 7).ToUpper();
                    //}

                    string responseMessage = rech.ResponseMessage;
                    string transactionId = rech.TransactionId;
                    string availableBalance = rech.AvailableBalance;
                    string resClientRefId = rech.ClientRefId;
                    string operatorTransactionId = rech.OperatorTransactionId;

                    int isUpdated = SMBPDataBase.UpdateBBPSPayment(lastId.ToString(), amount, transactionId, availableBalance, operatorTransactionId, responseMessage, true, "");

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static string BBPSPaymentEnquiry(string agentId, string clientRefId, string transactionId, string transStatus)
        {
            string result = string.Empty;

            try
            {
                string reqJson = "{\"ClientRefId\" :\"" + clientRefId + "\",\"TransactionId\" :\"" + transactionId + "\"}";
                result = Post("POST", (PostUrl + "BBPSPaymentEnquiry"), reqJson, "BBPSPaymentEnquiry", agentId, clientRefId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string clientRefId)
        {
            string ReturnValue = string.Empty;
            SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, clientRefId);

            StringBuilder sbResult = new StringBuilder();
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                Http.Headers.Add("ClientID", ClientID);
                Http.Headers.Add("SecretKey", SecretKey);
                Http.Headers.Add("TokenID", TokenID);

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                SMBPDataBase.Error("Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, clientRefId);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, clientRefId);
            return ReturnValue;
        }
        #endregion

        #region [Don't Remove this section]
        public static bool BBPSBillerList(string agentId)
        {
            bool isSuccess = false;

            try
            {
                StringBuilder sbResult = new StringBuilder();
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(PostUrl + "BBPSBillerList");

                Http.Headers.Add("ClientID", ClientID);
                Http.Headers.Add("SecretKey", SecretKey);
                Http.Headers.Add("TokenID", TokenID);
                Http.Timeout = 130000;
                Http.Accept = "application/json";
                Http.ContentType = "application/json";

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    Stream responseStream = WebResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                    sbResult.Append(reader.ReadToEnd());

                    BBPSBillerList bbpsBillerList = JsonConvert.DeserializeObject<BBPSBillerList>(sbResult.ToString());

                    isSuccess = SMBPDataBase.InsertBBPSBillerList(bbpsBillerList, agentId);

                    responseStream.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }
        #endregion

        public static string GetBillerLabel(string fetchid)
        {
            string result = string.Empty;

            DataTable dtLabel = SMBPDataBase.GetBillerLabel(fetchid);

            if (dtLabel != null && dtLabel.Rows.Count > 0)
            {
                List<BBPSBillerLabelData> labelList = new List<BBPSBillerLabelData>();

                List<string> indexcount = new List<string>();

                for (int i = 0; i < dtLabel.Rows.Count; i++)
                {
                    BBPSBillerLabelData tempLabel = new BBPSBillerLabelData();

                    if (!indexcount.Contains(dtLabel.Rows[i]["Index"].ToString()))
                    {
                        tempLabel.BillUpdation = dtLabel.Rows[i]["BillUpdation"].ToString();
                        tempLabel.Index = dtLabel.Rows[i]["Index"].ToString();
                        tempLabel.Labels = dtLabel.Rows[i]["Labels"].ToString().Replace(":", "").Trim();
                        tempLabel.FieldMinLen = dtLabel.Rows[i]["FieldMinLen"].ToString();
                        tempLabel.FieldMaxLen = dtLabel.Rows[i]["FieldMaxLen"].ToString();

                        indexcount.Add(dtLabel.Rows[i]["Index"].ToString());
                        labelList.Add(tempLabel);
                    }
                }

                var jsonrest = new JavaScriptSerializer().Serialize(labelList);
                result = jsonrest.ToString();
            }

            return result;
        }

        public static DataTable GetInputByLabelDel(string fetchid)
        {
            return SMBPDataBase.GetBillerLabel(fetchid); ;
        }

        public static DataTable GetBillTransactionHistory(string agentId, string fromDate = null, string toDate = null, string clintRefId = null, string transType = null, string status = null)
        {
            return SMBPDataBase.GetBillTransactionHistory(agentId, fromDate, toDate, clintRefId, transType, status);
        }

        public static bool ProcessToReFund(string transid, string reportid)
        {
            return SMBPDataBase.ProcessToReFund(transid, reportid);
        }

        public static bool UpdateBSSPPaymentByStatus(string billid, string transStatus)
        {
            return SMBPDataBase.UpdateBSSPPaymentByStatus(billid, transStatus);
        }

        //public static string BBPSPaymentEnquiry(string agentId, string clientRefId, string transactionId, string transStatus)
        //{
        //    string result = string.Empty;

        //    string grnUrl = PostUrl + "BBPSPaymentEnquiry";
        //    string reqJson = "{\"ClientRefId\" :\"" + clientRefId + "\",\"TransactionId\" :\"" + transactionId + "\",\"TransactionStatus\" :\"" + transStatus + "\"";

        //    try
        //    {
        //        //result = Post("POST", (PostUrl + "BBPSPaymentEnquiry"), reqJson, "BBPSPaymentEnquiry", agentId);


        //        SMBPDataBase.Information(grnUrl, "Request captured in post Method : " + grnUrl + "", reqJson, "", "BBPSPaymentEnquiry", agentId);

        //        var client = new RestClient(grnUrl);
        //        client.Timeout = 130000;
        //        var request = new RestRequest(Method.POST);
        //        request.AddHeader("ClientID", ClientID);
        //        request.AddHeader("SecretKey", SecretKey);
        //        request.AddHeader("TokenID", TokenID);
        //        request.AddHeader("Accept", "application/json");
        //        request.AddHeader("Content-Type", "application/json");
        //        request.AddParameter("application/json", reqJson, ParameterType.RequestBody);
        //        IRestResponse response = client.Execute(request);

        //        result = response.Content;
        //    }
        //    catch (Exception ex)
        //    {
        //        SMBPDataBase.Error("Request in Post Method : " + grnUrl + "", grnUrl, reqJson, "Response in Post Method : " + result + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", "BBPSPaymentEnquiry", agentId);
        //        result = null;
        //    }
        //    SMBPDataBase.Information(grnUrl, "Request captured in post Method : " + grnUrl + "", reqJson, result, "BBPSPaymentEnquiry", agentId);
        //    return result;
        //}
    }
}
